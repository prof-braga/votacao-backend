const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const requireDir = require("require-dir");
const bodyParser = require("body-parser");

mongoose.connect("mongodb+srv://usuario:senha@banco-sfvmn.mongodb.net/test?retryWrites=true&w=majority", { useCreateIndex: true, useNewUrlParser: true });

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const server = require("http").Server(app);
const io = require("socket.io")(server);

app.use((req, res, next) => {
  req.io = io;
  return next();
});

requireDir("./src/models");
app.use("/api", require("./src/routes"));

server.listen(process.env.PORT || 4000, () => {
  console.log("Servidor rodando na porta 4000");
});
